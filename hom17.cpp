﻿

#include <iostream>
using namespace std;

class Vector 
{

public:
    Vector() : x(0) , y(0) , z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show() 
    {
        cout << '\n' << x << ' ' << y << ' ' << z;

    }

private:
    double x;
    double y;
    double z;

};





int main()
{
    Vector v(30, 30, 50);
    v.Show();
}

